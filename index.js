const http = require("http");
const fs = require("fs");
const myServer = http.createServer((req, res) => {
  switch (req.url) {
    case "/GET/html":
      res.end(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);
      break;
    case "/GET/json":
      const data = {
        slideshow: {
          author: "Yours Truly",
          date: "date of publication",
          slides: [
            {
              title: "Wake up to WonderWidgets!",
              type: "all",
            },
            {
              items: [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets",
              ],
              title: "Overview",
              type: "all",
            },
          ],
          title: "Sample Slide Show",
        },
      };
      res.end(JSON.stringify(data));

    case "/GET/uuid":
      const uuid = require("uuid");
      const uuidV4 = uuid.v4();
      res.end(JSON.stringify(uuidV4));
      break;

    default:
      if (req.method === "GET") {
        const urlParts = req.url.split("/");
        if (urlParts[2] === "delay") {
          const delayInSeconds = parseInt(urlParts[3]);
          if (!isNaN(delayInSeconds)) {
            setTimeout(() => {
              res.writeHead(200, { "Content-Type": "text/plain" });
              res.end(`Success response after ${delayInSeconds} seconds delay`);
            }, delayInSeconds * 1000);
          } else {
            res.writeHead(400, { "Content-Type": "text/plain" });
            res.end("Invalid delay specified");
          }
        } else if (urlParts[2] === "status") {
          const statusCodeInReq = parseInt(urlParts[3]);
          if (!isNaN(statusCodeInReq)) {
            let checkStatus = http.STATUS_CODES[statusCodeInReq];
            res.writeHead(statusCodeInReq, { "Content-Type": "text/plain" });
            res.end(`Response with status code ${statusCodeInReq}:${checkStatus}`);
          } else {
            res.writeHead(400, { "Content-Type": "text/plain" });
            res.end("Invalid status code");
          }
        } else {
          res.end("404 not found");
        }
        break;
      }
  }
});
myServer.listen(8000, () => console.log("server started"));
